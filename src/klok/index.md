# Analoge klok

![Op een witte achtergrond staat een ronde analoge klok met drie wijzers.
De wijzerplaat heeft geen nummers en langere streepjes om het uur aan te duiden.
Links bovenaan staan de variabelen alarm uur en alarm minuten allebei met de waarde 0.  Links onder staat een knop zet alarm.](../img/klok.png)

Met een moderne computer een ouderwetse (analoge) klok tonen, dat is pas grappig.

Onze klok bevat:

* [x] drie wijzers: voor de uren, minuten en seconden
* [x] een wijzerplaat met streepjes
* [x] een knop om een alarmtijd in te stellen
* [x] een grappige sprite die elk uur tevoorschijn komt. Zoals in een koekoeksklok.

&nbsp;

<small>
[Creative Commons AttributionNonCommercial-ShareAlike 4.0 International License.](http://creativecommons.org/licenses/by-nc-sa/4.0/)
</small>

<small>
&copy; 2017 CoderDojo Sint Niklaas. Met dank aan CoderDojo Vorselaar voor het idee.
</small>

[Terug](../) [Stap 1 →](1.html)
