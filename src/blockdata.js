{
    "motion_movesteps": {
        "blockcode": "neem %n stappen"
    },
    "motion_turnright": {
        "blockcode": "draai @turnRight %n graden"
    },
    "motion_turnleft": {
        "blockcode": "draai @turnLeft %n graden"
    },
    "motion_goto": {
        "blockcode": "ga naar %r"
    },
    "motion_goto_menu": {
        "blockcode": ""
    },
    "motion_gotoxy": {
        "blockcode": "ga naar x: %n y: %n"
    },
    "motion_glideto": {
        "blockcode": "schuif in %n sec. naar %r"
    },
    "motion_glideto_menu": {
        "blockcode": ""
    },
    "motion_glidesecstoxy": {
        "blockcode": "schuif in %n sec. naar x: %n y: %n"
    },
    "motion_pointindirection": {
        "blockcode": "richt naar %n graden"
    },
    "motion_pointtowards": {
        "blockcode": "richt naar %r"
    },
    "motion_pointtowards_menu": {
        "blockcode": ""
    },
    "motion_changexby": {
        "blockcode": "verander x met %n"
    },
    "motion_setx": {
        "blockcode": "maak x %n"
    },
    "motion_changeyby": {
        "blockcode": "verander y met %n"
    },
    "motion_sety": {
        "blockcode": "maak y %n"
    },
    "motion_ifonedgebounce": {
        "blockcode": "keer om aan de rand"
    },
    "motion_setrotationstyle": {
        "blockcode": "maak draaistijl %m"
    },
    "looks_sayforsecs": {
        "blockcode": "zeg %s %n sec."
    },
    "looks_say": {
        "blockcode": "zeg %s"
    },
    "looks_thinkforsecs": {
        "blockcode": "denk %s %n sec."
    },
    "looks_think": {
        "blockcode": "denk %s"
    },
    "looks_switchcostumeto": {
        "blockcode": "verander uiterlijk naar %r"
    },
    "looks_costume": {
        "blockcode": ""
    },
    "looks_nextcostume": {
        "blockcode": "volgend uiterlijk"
    },
    "looks_switchbackdropto": {
        "blockcode": "verander achtergrond naar %r ::looks"
    },
    "looks_backdrops": {
        "blockcode": ""
    },
    "looks_nextbackdrop": {
        "blockcode": "volgend uiterlijk"
    },
    "looks_changesizeby": {
        "blockcode": "verander grootte met %n"
    },
    "looks_setsizeto": {
        "blockcode": "maak grootte %n"
    },
    "looks_changeeffectby": {
        "blockcode": "verander %m-effect met %n ::looks"
    },
    "looks_seteffectto": {
        "blockcode": "zet effect %m op %n ::looks"
    },
    "looks_cleargraphiceffects": {
        "blockcode": "zet alle effecten uit"
    },
    "looks_show": {
        "blockcode": "verschijn"
    },
    "looks_hide": {
        "blockcode": "verdwijn"
    },
    "looks_gotofrontback": {
        "blockcode": "ga naar laag %m ::looks"
    },
    "looks_goforwardbackwardlayers": {
        "blockcode": "ga %m %n lagen"
    },
    "sound_playuntildone": {
        "blockcode": "start geluid %r en wacht"
    },
    "sound_play": {
        "blockcode": "start geluid %r ::sound"
    },
    "sound_stopallsounds": {
        "blockcode": "stop alle geluiden"
    },
    "sound_changeeffectby": {
        "blockcode": "verander %m-effect met %n ::sound"
    },
    "sound_seteffectto": {
        "blockcode": "zet %m effect op %n ::sound"
    },
    "sound_cleareffects": {
        "blockcode": "zet alle effecten uit"
    },
    "sound_changevolumeby": {
        "blockcode": "verander volume met %n"
    },
    "sound_setvolumeto": {
        "blockcode": "verander volume met %n"
    },
    "event_whenflagclicked": {
        "blockcode": "wanneer groene vlag wordt aangeklikt"
    },
    "event_whenkeypressed": {
        "blockcode": "wanneer %m is ingedrukt"
    },
    "event_whenthisspriteclicked": {
        "blockcode": "wanneer op deze sprite wordt geklikt"
    },
    "event_whenbackdropswitchesto": {
        "blockcode": "wanneer achtergrond verandert naar %m"
    },
    "event_whengreaterthan": {
        "blockcode": "wanneer %m > %n"
    },
    "event_whenbroadcastreceived": {
        "blockcode": "wanneer ik signaal %m ontvang"
    },
    "event_broadcast": {
        "blockcode": "zend signaal %r"
    },
    "event_broadcastandwait": {
        "blockcode": "zend signaal %r en wacht"
    },
    "control_wait": {
        "blockcode": "wacht %n sec."
    },
    "control_repeat": {
        "blockcode": "herhaal %n {}"
    },
    "control_forever": {
        "blockcode": "herhaal {}"
    },
    "control_if": {
        "blockcode": "als %b dan {}"
    },
    "control_if_else": {
        "blockcode": "als %b dan {} anders {}"
    },
    "control_wait_until": {
        "blockcode": "wacht tot %b"
    },
    "control_repeat_until": {
        "blockcode": "herhaal tot %b {}"
    },
    "control_stop": {
        "blockcode": "stop %m"
    },
    "control_start_as_clone": {
        "blockcode": "wanneer ik als kloon start"
    },
    "control_create_clone_of": {
        "blockcode": "maak een kloon van %r"
    },
    "control_create_clone_of_menu": {
        "blockcode": ""
    },
    "control_delete_this_clone": {
        "blockcode": "verwijder deze kloon"
    },
    "motion_xposition": {
        "blockcode": "x-positie"
    },
    "motion_yposition": {
        "blockcode": "y-positie"
    },
    "motion_direction": {
        "blockcode": "richting"
    },
    "looks_costumenumbername": {
        "blockcode": "uiterlijk %m ::looks"
    },
    "looks_backdropnumbername": {
        "blockcode": "achtergrond %m ::looks"
    },
    "looks_size": {
        "blockcode": "grootte"
    },
    "sound_volume": {
        "blockcode": "volume"
    },
    "sensing_touchingobject": {
        "blockcode": "raakt ik %r ?"
    },
    "sensing_touchingobjectmenu": {
        "blockcode": ""
    },
    "sensing_touchingcolor": {
        "blockcode": "raak ik kleur %c ?"
    },
    "sensing_coloristouchingcolor": {
        "blockcode": "raakt kleur %c kleur %c ?"
    },
    "sensing_distanceto": {
        "blockcode": "afstand tot %r"
    },
    "sensing_distancetomenu": {
        "blockcode": ""
    },
    "sensing_askandwait": {
        "blockcode": "vraag %s en wacht"
    },
    "sensing_answer": {
        "blockcode": "antwoord"
    },
    "sensing_keypressed": {
        "blockcode": "toets %r ingedrukt?"
    },
    "sensing_keyoptions": {
        "blockcode": "%m"
    },
    "sensing_mousedown": {
        "blockcode": "muis ingedrukt?"
    },
    "sensing_mousex": {
        "blockcode": "muis x"
    },
    "sensing_mousey": {
        "blockcode": "muis y"
    },
    "sensing_setdragmode": {
        "blockcode": "zet sleepbaar op %m"
    },
    "sensing_loudness": {
        "blockcode": "volume"
    },
    "sensing_timer": {
        "blockcode": "klok"
    },
    "sensing_resettimer": {
        "blockcode": "zet klok op 0"
    },
    "sensing_of": {
        "blockcode": "%m van %r ::sensing"
    },
    "sensing_of_object_menu": {
        "blockcode": ""
    },
    "sensing_current": {
        "blockcode": "huidige %m"
    },
    "sensing_dayssince2000": {
        "blockcode": "dagen sinds 2000"
    },
    "sensing_username": {
        "blockcode": "gebruikersnaam"
    },
    "operator_add": {
        "blockcode": "%n + %n"
    },
    "operator_subtract": {
        "blockcode": "%n - %n"
    },
    "operator_multiply": {
        "blockcode": "%n * %n"
    },
    "operator_divide": {
        "blockcode": "%n / %n"
    },
    "operator_random": {
        "blockcode": "willekeurig getal tussen %n en %n"
    },
    "operator_gt": {
        "blockcode": "%n > %n"
    },
    "operator_lt": {
        "blockcode": "%n < %n"
    },
    "operator_equals": {
        "blockcode": "%n = %n"
    },
    "operator_and": {
        "blockcode": "%b and %b"
    },
    "operator_or": {
        "blockcode": "%b or %b"
    },
    "operator_not": {
        "blockcode": "niet %b"
    },
    "operator_join": {
        "blockcode": "voeg %s en %s samen"
    },
    "operator_letter_of": {
        "blockcode": "letter %n van %s"
    },
    "operator_length": {
        "blockcode": "lengte van %s"
    },
    "operator_contains": {
        "blockcode": "%s bevat %s ? "
    },
    "operator_mod": {
        "blockcode": "%n modulo %n"
    },
    "operator_round": {
        "blockcode": "afgerond %n"
    },
    "operator_mathop": {
        "blockcode": "%m van %n ::operators"
    },
    "data_setvariableto": {
        "blockcode": "maak %m %s"
    },
    "data_changevariableby": {
        "blockcode": "verander %m met %n"
    },
    "data_showvariable": {
        "blockcode": "toon variabele %m"
    },
    "data_hidevariable": {
        "blockcode": "verberg variabele %m"
    },
    "undefined": {
        "blockcode": "undefined"
    },
    "data_addtolist": {
        "blockcode": "voeg %s toe aan %m"
    },
    "data_deleteoflist": {
        "blockcode": "verwijder %n van %m"
    },
    "data_deletealloflist": {
        "blockcode": "verwijder alle van %m"
    },
    "data_insertatlist": {
        "blockcode": "voeg %s toe op %n van %m"
    },
    "data_replaceitemoflist": {
        "blockcode": "vervang item %n van %m door %s"
    },
    "data_itemoflist": {
        "blockcode": "item %n van %m"
    },
    "data_itemnumoflist": {
        "blockcode": "item # van %s in %m"
    },
    "data_lengthoflist": {
        "blockcode": "lengte van %m"
    },
    "data_listcontainsitem": {
        "blockcode": "%m bevat %s ? ::lists"
    },
    "data_showlist": {
        "blockcode": "toon lijst %m"
    },
    "data_hidelist": {
        "blockcode": "verberg lijst %m"
    }
}
