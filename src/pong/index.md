# Pong

![Het pong spel heeft een rode vloer, het pallet (de plank) is blauw, de achtergrond is wit
en in de bovenhoeken is het geel. Links onderaan is de score variabele weergegeven met de waarde 19.](../img/pong.png)

De beste manier om met Pong vertrouwd te raken is het eerst zelf te spelen!

<http://scratch.mit.edu/projects/48528496/>

<http://bit.ly/uhscratchstudio>

In dit project maak je kennis met de volgende concepten:

* [x] x- en y-coördinaten, richting van een sprite
* [x] genereren van willekeurige getallen
* [x] variabelen, condities, herhaling, als-dan-test
* [x] klonen, signalen, tijdklok, eigen blokken

Er zijn zes stappen.
Elke stap bestaat uit een opdracht en uitleg over nieuwe blokken.

&nbsp;

<small>
[Creative Commons AttributionNonCommercial-ShareAlike 4.0 International License.](http://creativecommons.org/licenses/by-nc-sa/4.0/)
</small>

<small>
&copy; <frank.neven@uhasselt.be>, <wim.lamotte@uhasselt.be>, <jonny.daenen@uhasselt.be>
</small>

[Terug](../) [Stap 1 →](1.html)
