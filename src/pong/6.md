# Stap 6

Opdracht:

1. zorg ervoor dat de tweede bal na 5 seconden verdwijnt
2. telkens er twee ballen zijn, wordt de plank langer
3. verdwijnt de tweede bal, dan wordt de plank terug korter

Nieuwe blokken:

<code class="b">(klok)</code>
<code class="b">zet klok op 0</code>
<code class="b">verander uiterlijk naar [plank-groot v]</code>
<code class="b">zend signaal [plank groot v]</code>
<code class="b">wanneer ik signaal [plank groot v] ontvang</code>
<code class="b">verwijder deze kloon</code>

## Tijdklok

<code class="b l">zet klok op 0</code>
zet de tijdklok op nul

<code class="b l"><(klok) > [5] ></code>
conditie wordt waar indien de waarde van de tijdklok groter is dan vijf

## Uiterlijken

<code class="b l">verander uiterlijk naar [plank-groot v]</code>
verandert uiterlijk van de sprite

## Signalen

<code class="b l">zend signaal [plank groot v]</code> stuurt het signaal *plank groot*

<code class="b s">wanneer ik signaal [plank groot v] ontvang</code>
wanneer deze sprite het signaal *plank groot* ontvangt dan worden
de blokken onder dit blok uitgevoerd

&nbsp;

<p class="testen">Test je programma!</p>

## Oplossing

**plank**

```scratch
wanneer groene vlag wordt aangeklikt
ga naar x: (0) y: (-120)
verander uiterlijk naar [plank-klein v]
herhaal
  maak x (muis x)

wanneer ik signaal [plank groot v] ontvang
verander uiterlijk naar [plank-groot v]

wanneer ik signaal [plank klein v] ontvang
verander uiterlijk naar [plank-klein v]
```

**bal**

```scratch
wanneer groene vlag wordt aangeklikt
maak grootte (50) %
ga naar x: (0) y: (130)
richt naar (willekeurig getal tussen (135) en (255)) graden
maak [aantalBallen] [1]
maak [score v] [0]
herhaal
  stuiter :: custom
  als << raak ik kleur [#4400ff] ?> en <(aantalBallen) = [1]>> dan
    zet klok op 0
    maak een kloon van [mijzelf v]
    maak [aantalBallen v] [2]
    zend signaal [plank groot v]
  einde
einde

wanneer ik als kloon start
maak grootte (50) %
ga naar x: (0) y: (130)
richt naar (willekeurig getal tussen (135) en (255)) graden
herhaal
  stuiter :: custom
  als <(klok) > [5]> dan
    maak [aantalBallen v][1]
    zend signaal [plank klein v]
    verwijder deze kloon
```


[← Stap 5](5.html) [Terug](../)
