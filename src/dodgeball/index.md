# Dodgeball

In dit spel moet je de ballen proberen ontwijken om zo de groene deur te bereiken!

![Er zijn 8 ballen te ontwijken in dit spel.
Het te besturen personage staat links onderaan op het gelijkvloers.
Er zijn nog twee verdiepingen, en de groene deur die we willen bereiken is rechts bovenaan.
](../img/dodgeball.png)

In dit project maak je kennis met de volgende concepten:

* [x] x- en y-coördinaten, richting van een sprite
* [x] genereren van willekeurige getallen
* [x] variabelen, condities, herhaling, als-dan-test
* [x] klonen
* [x] signalen

Er zijn 12 stappen.

<small>Naar voorbeeld van [CodeClub (pdf)](https://codeclub-project-assets.s3-eu-west-1.amazonaws.com/public/enGB/dodgeball.pdf)</small>

[Terug](../) [Stap 1 →](1.html)
