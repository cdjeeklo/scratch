# Vang het!

![Het vang spel heeft een uitzicht op een baai op zee.
Er is één appel in de lucht en één in een kom.
In het midden staat de tekst 'you win!'.
Bovenaan is een score variabele weergegeven met de waarde 10.
](../img/vangspel.png)

Maak een spel waarbij je dingen opvangt die uit de lucht vallen.

[Terug](../) [Stap 1 →](1.html)
