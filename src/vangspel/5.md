# Hou de score bij

Voeg een punt toe elke keer je de vallende sprite vangt.

1. Kies Variabelen
2. Klik op Maak een variabele
   ![Knop met de tekst "Maak een variabele".](../img/vangspel-5.1.png)
3. Geef de variabele de naam *Score* en klik OK

![Dialoog me de titel "Nieuwe variabele", met "Score" als naam, en "Voor alle sprites" is geselecteerd](../img/vangspel-5.2.png)


Voeg <code class="b l">maak [Score v] (0)</code> en
<code class="b l">verander [Score v] met (1)</code>
toe aan de code bij de appel:

```scratch
wanneer groene vlag wordt aangeklikt
maak [Score v] (0)
herhaal
  als < raak ik (Bowl v)?> dan
    start geluid (Pop v) en wacht
    verander [Score v] met (1)
    ga naar (willekeurige positie v)
    maak y (180)
```

<p class="testen">Test je code! Vang nu appels om punten te scoren!</p>

[← Stap 4](4.html) [Stap 6 →](6.html)
