# Schudden voor gebruik

Vond je het ook nogal makkelijk om te raden waar de dubbels lagen?
Nu gaan we de kaarten schudden...
Dat doen we door de elementen van onze lijst *kostuums* te verwisselen.

Maak 3 nieuwe variabelen aan, kies telkens *voor alle sprites*, en noem ze
<code class="b xl">(kostuumBuffer)</code>, <code class="b xl">(wissel1)</code> en <code class="b xl">(wissel2)</code>.

Ga terug naar de kaart sprite.

Maak een nieuw blok aan, en noem het *schudden*.
Deze keer vink je NIET aan *voer uit zonder het scherm te verversen*.

![Dialoog met de titel maak een blok. Er staat een blok met de naam schudden met invoer genaamd aantal.](../img/memory-8.png)

Klik ook op *voeg een invoer toe getal of tekst* en noem de invoer *aantal*.

<p class="testen">
Niet vergeten testen!
</p>

[← Stap 7](7.html) [Stap 9 →](9.html)
