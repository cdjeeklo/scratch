# Vals spelen verboden!

Je kan nu alle kaarten tegelijk omdraaien.
Zo is het niet moeilijk om te winnen!
Dat gaan we eens lekker tegenhouden...

Maak een nieuwe variabele en noem ze *reedsomgedraaid*.

Zet de variabele op 0 in het stukje bij *verstoppen*.

```scratch
wanneer ik signaal [verstoppen v] ontvang
wacht (1) sec.
verander uiterlijk naar (achterkant v)
maak [reedsomgedraaid v] (0)
```

De code om de kaart om te draaien wordt wat langer:

```scratch
wanneer op deze sprite wordt geklikt
als <(uiterlijk [naam v]) = (achterkant)> dan
  verander uiterlijk naar (item (MijnKaartNummer) van [kostuums v])
  als <(reedsomgedraaid) = (0)> dan
    maak [reedsomgedraaid v] (mijnkaartnummer)
  anders
    als <(item (reedsomgedraaid) van [kostuums v]) = (item (MijnKaartNummer) van [kostuums v])> dan
      maak [reedsomgedraaid v] (0)
      start geluid (High Whoosh v)
      zend signaal (match v)
    anders
      start geluid (Pew v)
      zend signaal (verstoppen v)
```

Kies zelf 2 geluidjes...

<p class="testen">
Niet vergeten testen!
</p>

[← Stap 5](5.html) [Stap 7 →](7.html)
