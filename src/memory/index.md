# Memory

<p align="center">
<img class="preview" src="../img/memory.png"
alt="Het spel memory. Op een witte achtergrond staan plaatjes door elkaar in een rooster. Er zijn telkens twee plaatjes van dezelfde figuur. Links bovenaan staat een variabele kaarten te gaan met de waaarde 24."/>
</p>

Dit spel gebruikt:

* [x] clones
* [x] variabelen
* [x] lijsten
* [x] signalen
* [x] zelfgemaakte blokjes

[Terug](../) [Stap 1 →](1.html)
