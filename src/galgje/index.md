# Galgje

![Op een veld staat een tekening man een ventje dat aan de galg hangt. Op de grond liggen de letters U, I, E, E en A. In de lucht staan de letters L, L, K, K, R, L, N en D.](../img/galgje.png)

Dit spel gebruikt:

* [x] clones
* [x] variabelen
* [x] lijsten
* [x] signalen

[Terug](../) [Stap 1 →](1.html)
