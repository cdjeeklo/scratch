# Flappy Bat

![In het Flappy Bat spel is een vleermuis die door een griezelig bos vliegt. We zien een dwarsdoorsnede van een muur met een gat waartussen de vleermuis moet proberen vliegen. Links bovenaan is de score variabele weergegeven met de waarde 2.](../img/flappybat.png)

De vleermuis moet door de
gaten in de muren vliegen.
Per overwonnen muur krijg je 1 punt!

Voer het programma zelf eens uit!

<http://scratch.mit.edu/projects/50502446/>

<http://bit.ly/uhscratchstudio>

In dit project maak je kennis met de volgende concepten:

* [x] x- en y-coördinaten
* [x] herhalingen en condities
* [x] uiterlijk van een sprite aanpassen
* [x] botsingen detecteren
* [x] variabelen gebruiken

Er zijn vijf stappen.

Elke stap bestaat uit een opdracht en uitleg over nieuwe blokken.

&nbsp;

<small>
[Creative Commons AttributionNonCommercial-ShareAlike 4.0 International License.](http://creativecommons.org/licenses/by-nc-sa/4.0/)
</small>

<small>
&copy; <frank.neven@uhasselt.be>, <wim.lamotte@uhasselt.be>, <jonny.daenen@uhasselt.be>
</small>

[Terug](../) [Stap 1 →](1.html)
