# Stap 1

Opdracht:

1. laat de **muur** van rechts naar links over het scherm bewegen
2. zorg dat de muur terugspringt naar rechts

Blokken:

<code class="b">wanneer groene vlag wordt aangeklikt</code>
<code class="b">herhaal</code>
<code class="b">herhaal tot <></code>
<code class="b">verander x met (-5)</code>
<code class="b">maak x (240)</code>
<code class="b">(x-positie)</code>
<code class="b"><[] < [-220]></code>

Een **controle blok** beïnvloed de uitvoering van het programma.

<code class="b s">wanneer groene vlag wordt aangeklikt</code>
alle blokken onder dit blok worden uitgevoerd
wanneer op de groene vlag wordt geklikt

<code class="b">herhaal</code>
alle blokken in dit blok worden telkens opnieuw uitgevoerd
(tot het programma gestopt wordt)

<code class="b">herhaal tot <></code>
alle blokken in dit blok worden telkens opnieuw uitgevoerd
totdat een bepaalde voorwaarde voldaan is

Een **opdracht** block voert een opdracht uit.

<code class="b l">maak x (240)</code> 
plaats de sprite op de gegeven x-positie

<code class="b l">verander x met (-5)</code>
verander de x-positie van de sprite

Een **functie** block geeft een waarde terug.

<code class="b xl">(x-positie)</code>
geeft de x-positie van de sprite terug

**conditie** is waar of onwaar

<code class="b xl"><[] < [-220]></code>
*waar* als de vergelijking klopt (vb. `1 < 2`)

<p class="testen">Test je programma!</p>

## Oplossing

```scratch
wanneer groene vlag wordt aangeklikt
herhaal
  maak x (240)
  herhaal tot <[-220] > (x-positie)>
    verander x met (-5)
```

[← Overzicht](index.html) [Stap 2 →](2.html)
